﻿set search_path to prueba;

INSERT INTO anio values ('01','1994'),
			('02','1995'),
			('03','1996'),
			('04','1997'),
			('05','1998'),
			('06','1999'),
			('07','2000'),
			('08','2001'),
			('09','2002'),
			('10','2003'),
			('11','2004'),
			('12','2005'),
			('13','2006'),
			('14','2007'),
			('15','2008'),
			('16','2009'),
			('17','2010'),
			('18','2011'),
			('19','2012'),
			('20','2013');

INSERT INTO entidad values ('00','Nacional');

INSERT INTO municipio values ('00','00','Nacional');

INSERT INTO indicador VALUES ('1002000026','Nacimientos'),
			     ('1002000027','Nacimientos hombres'),
			     ('1002000028','Nacimientos mujeres'),
			     ('1002000029','Nacimientos de sexo no especificado'),
			     ('1002000030','Defunciones generales'),
			     ('1002000031','Defunciones generales hombres'),
			     ('1002000032','Defunciones genereales mujeres'),
			     ('1002000033','Defunciones generales de sexo no especificado'),
			     ('1002000034','Defunciones de menores de un anio'),
			     ('1002000035','Defunciones de menores de un anio hombres'),
			     ('1002000036','Defunciones de menores de un anio mujeres'),
			     ('1002000037','Defunciones de menores de un anio de sexo no especificado'),
			     ('1002000038','Matrimonios'),
			     ('1002000039','Divorcios');

INSERT INTO nivel VALUES ('01','Natalidad y fecundidad'),
			 ('02','Mortalidad'),
			 ('03','Nupcialidad');

INSERT INTO subnivel VALUES ('01','101','Natalidad'),
			    ('02','201','Mortalidad'),
			    ('03','301','Matrimonios'),
			    ('03','302','Divorcios');

select * from tasa_poblacional;

insert into tasa_poblacional values ('00001',	'01',	'1002000026'	,	'2904389'	,	'00',	'101'	);
insert into tasa_poblacional values ('00002',	'01',	'1002000027'	,	'1462458'	,	'00',	'101'	);
insert into tasa_poblacional values ('00003',	'01',	'1002000028'	,	'1441367'	,	'00',	'101'	);
insert into tasa_poblacional values ('00004',	'01',	'1002000029'	,	'564'		,	'00',	'101'	);
insert into tasa_poblacional values ('00005',	'01',	'1002000030'	,	'419074'	,	'00',	'201'	);
insert into tasa_poblacional values ('00006',	'01',	'1002000031'	,	'237772'	,	'00',	'201'	);
insert into tasa_poblacional values ('00007',	'01',	'1002000032'	,	'181136'	,	'00',	'201'	);
insert into tasa_poblacional values ('00008',	'01',	'1002000033'	,	'166'		,	'00',	'201'	);
insert into tasa_poblacional values ('00009',	'01',	'1002000034'	,	'49305'  	,	'00',	'201'	);
insert into tasa_poblacional values ('00010',	'01',	'1002000035'	,	'28038'		,	'00',	'201'	);
insert into tasa_poblacional values ('00011',	'01',	'1002000036'	,	'21192'		,	'00',	'201'	);
insert into tasa_poblacional values ('00012',	'01',	'1002000037'	,	'75'		,	'00',	'201'	);
insert into tasa_poblacional values ('00013',	'01',	'1002000038'	,	'671640'	,	'00',	'301'	);
insert into tasa_poblacional values ('00014',	'01',	'1002000039'	,	'35029'		,	'00',	'302'	);

insert into tasa_poblacional values ('00015',	'02',	'1002000026'	,	'2750444'	,	'00',	'101'	);
insert into tasa_poblacional values ('00016',	'02',	'1002000027'	,	'1387458'	,	'00',	'101'	);
insert into tasa_poblacional values ('00017',	'02',	'1002000028'	,	'1362547'	,	'00',	'101'	);
insert into tasa_poblacional values ('00018',	'02',	'1002000029'	,	'439'		,	'00',	'101'	);
insert into tasa_poblacional values ('00019',	'02',	'1002000030'	,	'430278'	,	'00',	'201'	);
insert into tasa_poblacional values ('00020',	'02',	'1002000031'	,	'242408'	,	'00',	'201'	);
insert into tasa_poblacional values ('00021',	'02',	'1002000032'	,	'187693'	,	'00',	'201'	);
insert into tasa_poblacional values ('00022',	'02',	'1002000033'	,	'177'		,	'00',	'201'	);
insert into tasa_poblacional values ('00023',	'02',	'1002000034'	,	'48023'		,	'00',	'201'	);
insert into tasa_poblacional values ('00024',	'02',	'1002000035'	,	'27237'		,	'00',	'201'	);
insert into tasa_poblacional values ('00025',	'02',	'1002000036'	,	'20718'		,	'00',	'201'	);
insert into tasa_poblacional values ('00026',	'02',	'1002000037'	,	'68'		,	'00',	'201'	);
insert into tasa_poblacional values ('00027',	'02',	'1002000038'	,	'658114'	,	'00',	'301'	);
insert into tasa_poblacional values ('00028',	'02',	'1002000039'	,	'37455'		,	'00',	'302'	);


insert into tasa_poblacional values ('00029',	'03',	'1002000026'	,	'2707718'	,'00',	'101'	);
insert into tasa_poblacional values ('00030',	'03',	'1002000027'	,	'1365863'	,'00',	'101'	);
insert into tasa_poblacional values ('00031',	'03',	'1002000028'	,	'1341619'	,'00',	'101'	);
insert into tasa_poblacional values ('00032',	'03',	'1002000029'	,	'236'		,'00',	'101'	);
insert into tasa_poblacional values ('00033',	'03',	'1002000030'	,	'436321'	,'00',	'201'	);
insert into tasa_poblacional values ('00034',	'03',	'1002000031'	,	'245017'	,'00',	'201'	);
insert into tasa_poblacional values ('00035',	'03',	'1002000032'	,	'191168'	,'00',	'201'	);
insert into tasa_poblacional values ('00036',	'03',	'1002000033'	,	'136'		,'00',	'201'	);
insert into tasa_poblacional values ('00037',	'03',	'1002000034'	,	'45707'		,'00',	'201'	);
insert into tasa_poblacional values ('00038',	'03',	'1002000035'	,	'25916'		,'00',	'201'	);
insert into tasa_poblacional values ('00039',	'03',	'1002000036'	,	'19726'		,'00',	'201'	);
insert into tasa_poblacional values ('00040',	'03',	'1002000037'	,	'65'		,'00',	'201'	);
insert into tasa_poblacional values ('00041',	'03',	'1002000038'	,	'670523'	,'00',	'301'	);
insert into tasa_poblacional values ('00042',	'03',	'1002000039'	,	'38545'		,'00',	'302'	);

insert into tasa_poblacional values ('00043','04',	'1002000026'	,	'2698425'	,'00',	'101'	);
insert into tasa_poblacional values ('00044','04',	'1002000027'	,	'1359672'	,'00',	'101'	);
insert into tasa_poblacional values ('00045','04',	'1002000028'	,	'1338539'	,'00',	'101'	);
insert into tasa_poblacional values ('00046','04',	'1002000029'	,	'214'		,'00',	'101'	);
insert into tasa_poblacional values ('00047','04',	'1002000030'	,	'440437'	,'00',	'201'	);
insert into tasa_poblacional values ('00048','04',	'1002000031'	,	'247318'	,'00',	'201'	);
insert into tasa_poblacional values ('00049','04',	'1002000032'	,	'192941'	,'00',	'201'	);
insert into tasa_poblacional values ('00050','04',	'1002000033'	,	'178'		,'00',	'201'	);
insert into tasa_poblacional values ('00051','04',	'1002000034'	,	'44377'		,'00',	'201'	);
insert into tasa_poblacional values ('00052','04',	'1002000035'	,	'25165'		,'00',	'201'	);
insert into tasa_poblacional values ('00053','04',	'1002000036'	,	'19145'		,'00',	'201'	);
insert into tasa_poblacional values ('00054','04',	'1002000037'	,	'67'		,'00',	'201'	);
insert into tasa_poblacional values ('00055','04',	'1002000038'	,	'707840'	,'00',	'301'	);
insert into tasa_poblacional values ('00056','04',	'1002000039'	,	'40792'		,'00',	'302'	);


insert into tasa_poblacional values ('00057','05',	'1002000026'	,	'2668428'	,'00',	'101'	);
insert into tasa_poblacional values ('00058','05',	'1002000027'	,	'1345837'	,'00',	'101'	);
insert into tasa_poblacional values ('00059','05',	'1002000028'	,	'1322244'	,'00',	'101'	);
insert into tasa_poblacional values ('00060','05',	'1002000029'	,	'347'		,'00',	'101'	);
insert into tasa_poblacional values ('00061','05',	'1002000030'	,	'444665'	,'00',	'201'	);
insert into tasa_poblacional values ('00062','05',	'1002000031'	,	'249030'	,'00',	'201'	);
insert into tasa_poblacional values ('00063','05',	'1002000032'	,	'195460'	,'00',	'201'	);
insert into tasa_poblacional values ('00064','05',	'1002000033'	,	'175'		,'00',	'201'	);
insert into tasa_poblacional values ('00065','05',	'1002000034'	,	'42183'		,'00',	'201'	);
insert into tasa_poblacional values ('00066','05',	'1002000035'	,	'23557'		,'00',	'201'	);
insert into tasa_poblacional values ('00067','05',	'1002000036'	,	'18548'		,'00',	'201'	);
insert into tasa_poblacional values ('00068','05',	'1002000037'	,	'78'		,'00',	'201'	);
insert into tasa_poblacional values ('00069','05',	'1002000038'	,	'704456'	,'00',	'301'	);
insert into tasa_poblacional values ('00070','05',	'1002000039'	,	'45889'		,'00',	'302'	);

insert into tasa_poblacional values ('00071','06',	'1002000026'	,	'2769089'	,'00',	'101'	);
insert into tasa_poblacional values ('00072','06',	'1002000027'	,	'1384810'	,'00',	'101'	);
insert into tasa_poblacional values ('00073','06',	'1002000028'	,	'1384000'	,'00',	'101'	);
insert into tasa_poblacional values ('00074','06',	'1002000029'	,	'279'		,'00',	'101'	);
insert into tasa_poblacional values ('00075','06',	'1002000030'	,	'443950'	,'00',	'201'	);
insert into tasa_poblacional values ('00076','06',	'1002000031'	,	'247833'	,'00',	'201'	);
insert into tasa_poblacional values ('00077','06',	'1002000032'	,	'195979'	,'00',	'201'	);
insert into tasa_poblacional values ('00078','06',	'1002000033'	,	'138'		,'00',	'201'	);
insert into tasa_poblacional values ('00079','06',	'1002000034'	,	'40283'		,'00',	'201'	);
insert into tasa_poblacional values ('00080','06',	'1002000035'	,	'22754'		,'00',	'201'	);
insert into tasa_poblacional values ('00081','06',	'1002000036'	,	'17463'		,'00',	'201'	);
insert into tasa_poblacional values ('00082','06',	'1002000037'	,	'66'		,'00',	'201'	);
insert into tasa_poblacional values ('00083','06',	'1002000038'	,	'743856'	,'00',	'301'	);
insert into tasa_poblacional values ('00084','06',	'1002000039'	,	'49271'		,'00',	'302'	);
						
insert into tasa_poblacional values ('00085','07',	'1002000026'	,	'2798339'	,'00',	'101'	);
insert into tasa_poblacional values ('00086','07',	'1002000027'	,	'1398877'	,'00',	'101'	);
insert into tasa_poblacional values ('00087','07',	'1002000028'	,	'1398703'	,'00',	'101'	);
insert into tasa_poblacional values ('00088','07',	'1002000029'	,	'759'		,'00',	'101'	);
insert into tasa_poblacional values ('00089','07',	'1002000030'	,	'437667'	,'00',	'201'	);
insert into tasa_poblacional values ('00090','07',	'1002000031'	,	'244302'	,'00',	'201'	);
insert into tasa_poblacional values ('00091','07',	'1002000032'	,	'193253'	,'00',	'201'	);
insert into tasa_poblacional values ('00092','07',	'1002000033'	,	'112'		,'00',	'201'	);
insert into tasa_poblacional values ('00093','07',	'1002000034'	,	'38621'		,'00',	'201'	);
insert into tasa_poblacional values ('00094','07',	'1002000035'	,	'21793'		,'00',	'201'	);
insert into tasa_poblacional values ('00095','07',	'1002000036'	,	'16769'		,'00',	'201'	);
insert into tasa_poblacional values ('00096','07',	'1002000037'	,	'59'		,'00',	'201'	);
insert into tasa_poblacional values ('00097','07',	'1002000038'	,	'707422'	,'00',	'301'	);
insert into tasa_poblacional values ('00098','07',	'1002000039'	,	'52358'		,'00',	'302'	);
						
insert into tasa_poblacional values ('00099','08',	'1002000026'	,	'2767610'	,'00',	'101'	);
insert into tasa_poblacional values ('00100','08',	'1002000027'	,	'1390066'	,'00',	'101'	);
insert into tasa_poblacional values ('00101','08',	'1002000028'	,	'1377151'	,'00',	'101'	);
insert into tasa_poblacional values ('00102','08',	'1002000029'	,	'393'		,'00',	'101'	);
insert into tasa_poblacional values ('00103','08',	'1002000030'	,	'443127'	,'00',	'201'	);
insert into tasa_poblacional values ('00104','08',	'1002000031'	,	'245998'	,'00',	'201'	);
insert into tasa_poblacional values ('00105','08',	'1002000032'	,	'196789'	,'00',	'201'	);
insert into tasa_poblacional values ('00106','08',	'1002000033'	,	'340'		,'00',	'201'	);
insert into tasa_poblacional values ('00107','08',	'1002000034'	,	'35911'		,'00',	'201'	);
insert into tasa_poblacional values ('00108','08',	'1002000035'	,	'20302'		,'00',	'201'	);
insert into tasa_poblacional values ('00109','08',	'1002000036'	,	'15487'		,'00',	'201'	);
insert into tasa_poblacional values ('00110','08',	'1002000037'	,	'122'		,'00',	'201'	);
insert into tasa_poblacional values ('00111','08',	'1002000038'	,	'665434'	,'00',	'301'	);
insert into tasa_poblacional values ('00112','08',	'1002000039'	,	'57370'		,'00',	'302'	);

insert into tasa_poblacional values ('00113','09',	'1002000026'	,	'2699084'	,'00',	'101'	);
insert into tasa_poblacional values ('00114','09',	'1002000027'	,	'1345504'	,'00',	'101'	);
insert into tasa_poblacional values ('00115','09',	'1002000028'	,	'1350142'	,'00',	'101'	);
insert into tasa_poblacional values ('00116','09',	'1002000029'	,	'3438'		,'00',	'101'	);
insert into tasa_poblacional values ('00117','09',	'1002000030'	,	'459687'	,'00',	'201'	);
insert into tasa_poblacional values ('00118','09',	'1002000031'	,	'255522'	,'00',	'201'	);
insert into tasa_poblacional values ('00119','09',	'1002000032'	,	'203846'	,'00',	'201'	);
insert into tasa_poblacional values ('00120','09',	'1002000033'	,	'319'		,'00',	'201'	);
insert into tasa_poblacional values ('00121','09',	'1002000034'	,	'36567'		,'00',	'201'	);
insert into tasa_poblacional values ('00122','09',	'1002000035'	,	'20734'		,'00',	'201'	);
insert into tasa_poblacional values ('00123','09',	'1002000036'	,	'15690'		,'00',	'201'	);
insert into tasa_poblacional values ('00124','09',	'1002000037'	,	'143'		,'00',	'201'	);
insert into tasa_poblacional values ('00125','09',	'1002000038'	,	'616654'	,'00',	'301'	);
insert into tasa_poblacional values ('00126','09',	'1002000039'	,	'60641'		,'00',	'302'	);
						
insert into tasa_poblacional values ('00127','10',	'1002000026'	,	'2655894'	,'00',	'101'	);
insert into tasa_poblacional values ('00128','10',	'1002000027'	,	'1307080'	,'00',	'101'	);
insert into tasa_poblacional values ('00129','10',	'1002000028'	,	'1348354'	,'00',	'101'	);
insert into tasa_poblacional values ('00130','10',	'1002000029'	,	'460'		,'00',	'101'	);
insert into tasa_poblacional values ('00131','10',	'1002000030'	,	'472140'	,'00',	'201'	);
insert into tasa_poblacional values ('00132','10',	'1002000031'	,	'261680'	,'00',	'201'	);
insert into tasa_poblacional values ('00133','10',	'1002000032'	,	'210096'	,'00',	'201'	);
insert into tasa_poblacional values ('00134','10',	'1002000033'	,	'364'		,'00',	'201'	);
insert into tasa_poblacional values ('00135','10',	'1002000034'	,	'33355'		,'00',	'201'	);
insert into tasa_poblacional values ('00136','10',	'1002000035'	,	'19008'		,'00',	'201'	);
insert into tasa_poblacional values ('00137','10',	'1002000036'	,	'14236'		,'00',	'201'	);
insert into tasa_poblacional values ('00138','10',	'1002000037'	,	'111'		,'00',	'201'	);
insert into tasa_poblacional values ('00139','10',	'1002000038'	,	'584142'	,'00',	'301'	);
insert into tasa_poblacional values ('00140','10',	'1002000039'	,	'64248'		,'00',	'302'	);
						
insert into tasa_poblacional values ('00141','11',	'1002000026'	,	'2625056'	,'00',	'101'	);
insert into tasa_poblacional values ('00142','11',	'1002000027'	,	'1302411'	,'00',	'101'	);
insert into tasa_poblacional values ('00143','11',	'1002000028'	,	'1322074'	,'00',	'101'	);
insert into tasa_poblacional values ('00144','11',	'1002000029'	,	'571'		,'00',	'101'	);
insert into tasa_poblacional values ('00145','11',	'1002000030'	,	'473417'	,'00',	'201'	);
insert into tasa_poblacional values ('00146','11',	'1002000031'	,	'261919'	,'00',	'201'	);
insert into tasa_poblacional values ('00147','11',	'1002000032'	,	'211294'	,'00',	'201'	);
insert into tasa_poblacional values ('00148','11',	'1002000033'	,	'204'		,'00',	'201'	);
insert into tasa_poblacional values ('00149','11',	'1002000034'	,	'32764'		,'00',	'201'	);
insert into tasa_poblacional values ('00150','11',	'1002000035'	,	'18524'		,'00',	'201'	);
insert into tasa_poblacional values ('00151','11',	'1002000036'	,	'14163'		,'00',	'201'	);
insert into tasa_poblacional values ('00152','11',	'1002000037'	,	'77'		,'00',	'201'	);
insert into tasa_poblacional values ('00153','11',	'1002000038'	,	'600563'	,'00',	'301'	);
insert into tasa_poblacional values ('00154','11',	'1002000039'	,	'67575'		,'00',	'302'	);


insert into tasa_poblacional values ('00155','12',	'1002000026'	,	'2567906'	,'00',	'101'	);
insert into tasa_poblacional values ('00156','12',	'1002000027'	,	'1284304'	,'00',	'101'	);
insert into tasa_poblacional values ('00157','12',	'1002000028'	,	'1283009'	,'00',	'101'	);
insert into tasa_poblacional values ('00158','12',	'1002000029'	,	'593'		,'00',	'101'	);
insert into tasa_poblacional values ('00159','12',	'1002000030'	,	'495240'	,'00',	'201'	);
insert into tasa_poblacional values ('00160','12',	'1002000031'	,	'273126'	,'00',	'201'	);
insert into tasa_poblacional values ('00161','12',	'1002000032'	,	'221968'	,'00',	'201'	);
insert into tasa_poblacional values ('00162','12',	'1002000033'	,	'146'		,'00',	'201'	);
insert into tasa_poblacional values ('00163','12',	'1002000034'	,	'32603'		,'00',	'201'	);
insert into tasa_poblacional values ('00164','12',	'1002000035'	,	'18214'		,'00',	'201'	);
insert into tasa_poblacional values ('00165','12',	'1002000036'	,	'14318'		,'00',	'201'	);
insert into tasa_poblacional values ('00166','12',	'1002000037'	,	'71'		,'00',	'201'	);
insert into tasa_poblacional values ('00167','12',	'1002000038'	,	'595713'	,'00',	'301'	);
insert into tasa_poblacional values ('00168','12',	'1002000039'	,	'70184'		,'00',	'302'	);
						
insert into tasa_poblacional values ('00169','13',	'1002000026'	,	'2505939'	,'00',	'101'	);
insert into tasa_poblacional values ('00170','13',	'1002000027'	,	'1254600'	,'00',	'101'	);
insert into tasa_poblacional values ('00171','13',	'1002000028'	,	'1250937'	,'00',	'101'	);
insert into tasa_poblacional values ('00172','13',	'1002000029'	,	'402'		,'00',	'101'	);
insert into tasa_poblacional values ('00173','13',	'1002000030'	,	'494471'	,'00',	'201'	);
insert into tasa_poblacional values ('00174','13',	'1002000031'	,	'274091'	,'00',	'201'	);
insert into tasa_poblacional values ('00175','13',	'1002000032'	,	'220240'	,'00',	'201'	);
insert into tasa_poblacional values ('00176','13',	'1002000033'	,	'140'		,'00',	'201'	);
insert into tasa_poblacional values ('00177','13',	'1002000034'	,	'30899'		,'00',	'201'	);
insert into tasa_poblacional values ('00178','13',	'1002000035'	,	'17373'		,'00',	'201'	);
insert into tasa_poblacional values ('00179','13',	'1002000036'	,	'13447'		,'00',	'201'	);
insert into tasa_poblacional values ('00180','13',	'1002000037'	,	'79'		,'00',	'201'	);
insert into tasa_poblacional values ('00181','13',	'1002000038'	,	'586978'	,'00',	'301'	);
insert into tasa_poblacional values ('00182','13',	'1002000039'	,	'72396'		,'00',	'302'	);
						
insert into tasa_poblacional values ('00183','14',	'1002000026'	,	'2655083'	,'00',	'101'	);
insert into tasa_poblacional values ('00184','14',	'1002000027'	,	'1330390'	,'00',	'101'	);
insert into tasa_poblacional values ('00185','14',	'1002000028'	,	'1324087'	,'00',	'101'	);
insert into tasa_poblacional values ('00186','14',	'1002000029'	,	'606'		,'00',	'101'	);
insert into tasa_poblacional values ('00187','14',	'1002000030'	,	'514420'	,'00',	'201'	);
insert into tasa_poblacional values ('00188','14',	'1002000031'	,	'284910'	,'00',	'201'	);
insert into tasa_poblacional values ('00189','14',	'1002000032'	,	'229336'	,'00',	'201'	);
insert into tasa_poblacional values ('00190','14',	'1002000033'	,	'174'		,'00',	'201'	);
insert into tasa_poblacional values ('00191','14',	'1002000034'	,	'30425'		,'00',	'201'	);
insert into tasa_poblacional values ('00192','14',	'1002000035'	,	'17190'		,'00',	'201'	);
insert into tasa_poblacional values ('00193','14',	'1002000036'	,	'13142'		,'00',	'201'	);
insert into tasa_poblacional values ('00194','14',	'1002000037'	,	'93'		,'00',	'201'	);
insert into tasa_poblacional values ('00195','14',	'1002000038'	,	'595209'	,'00',	'301'	);
insert into tasa_poblacional values ('00196','14',	'1002000039'	,	'77255'		,'00',	'302'	);

insert into tasa_poblacional values ('00197','15',	'1002000026'	,	'2636110'	,'00',	'101'	);
insert into tasa_poblacional values ('00198','15',	'1002000027'	,	'1320177'	,'00',	'101'	);
insert into tasa_poblacional values ('00199','15',	'1002000028'	,	'1315435'	,'00',	'101'	);
insert into tasa_poblacional values ('00200','15',	'1002000029'	,	'498'		,'00',	'101'	);
insert into tasa_poblacional values ('00201','15',	'1002000030'	,	'539530'	,'00',	'201'	);
insert into tasa_poblacional values ('00202','15',	'1002000031'	,	'300837'	,'00',	'201'	);
insert into tasa_poblacional values ('00203','15',	'1002000032'	,	'238523'	,'00',	'201'	);
insert into tasa_poblacional values ('00204','15',	'1002000033'	,	'170'		,'00',	'201'	);
insert into tasa_poblacional values ('00205','15',	'1002000034'	,	'29537'		,'00',	'201'	);
insert into tasa_poblacional values ('00206','15',	'1002000035'	,	'16585'		,'00',	'201'	);
insert into tasa_poblacional values ('00207','15',	'1002000036'	,	'12851'		,'00',	'201'	);
insert into tasa_poblacional values ('00208','15',	'1002000037'	,	'101'		,'00',	'201'	);
insert into tasa_poblacional values ('00209','15',	'1002000038'	,	'589352'	,'00',	'301'	);
insert into tasa_poblacional values ('00210','15',	'1002000039'	,	'81851'		,'00',	'302'	);
						
insert into tasa_poblacional values ('00211','16',	'1002000026'	,	'2577214'	,'00',	'101'	);
insert into tasa_poblacional values ('00212','16',	'1002000027'	,	'1296770'	,'00',	'101'	);
insert into tasa_poblacional values ('00213','16',	'1002000028'	,	'1279883'	,'00',	'101'	);
insert into tasa_poblacional values ('00214','16',	'1002000029'	,	'561'		,'00',	'101'	);
insert into tasa_poblacional values ('00215','16',	'1002000030'	,	'564673'	,'00',	'201'	);
insert into tasa_poblacional values ('00216','16',	'1002000031'	,	'316058'	,'00',	'201'	);
insert into tasa_poblacional values ('00217','16',	'1002000032'	,	'248371'	,'00',	'201'	);
insert into tasa_poblacional values ('00218','16',	'1002000033'	,	'244'		,'00',	'201'	);
insert into tasa_poblacional values ('00219','16',	'1002000034'	,	'28988'		,'00',	'201'	);
insert into tasa_poblacional values ('00220','16',	'1002000035'	,	'16231'		,'00',	'201'	);
insert into tasa_poblacional values ('00221','16',	'1002000036'	,	'12697'		,'00',	'201'	);
insert into tasa_poblacional values ('00222','16',	'1002000037'	,	'60'		,'00',	'201'	);
insert into tasa_poblacional values ('00223','16',	'1002000038'	,	'558913'	,'00',	'301'	);
insert into tasa_poblacional values ('00224','16',	'1002000039'	,	'84302'		,'00',	'302'	);
						
insert into tasa_poblacional values ('00225','17',	'1002000026'	,	'2643908'	,'00',	'101'	);
insert into tasa_poblacional values ('00226','17',	'1002000027'	,	'1326612'	,'00',	'101'	);
insert into tasa_poblacional values ('00227','17',	'1002000028'	,	'1317023'	,'00',	'101'	);
insert into tasa_poblacional values ('00228','17',	'1002000029'	,	'273'		,'00',	'101'	);
insert into tasa_poblacional values ('00229','17',	'1002000030'	,	'592018'	,'00',	'201'	);
insert into tasa_poblacional values ('00230','17',	'1002000031'	,	'332027'	,'00',	'201'	);
insert into tasa_poblacional values ('00231','17',	'1002000032'	,	'259669'	,'00',	'201'	);
insert into tasa_poblacional values ('00232','17',	'1002000033'	,	'322'		,'00',	'201'	);
insert into tasa_poblacional values ('00233','17',	'1002000034'	,	'28865'		,'00',	'201'	);
insert into tasa_poblacional values ('00234','17',	'1002000035'	,	'16148'		,'00',	'201'	);
insert into tasa_poblacional values ('00235','17',	'1002000036'	,	'12637'		,'00',	'201'	);
insert into tasa_poblacional values ('00236','17',	'1002000037'	,	'80'		,'00',	'201'	);
insert into tasa_poblacional values ('00237','17',	'1002000038'	,	'568632'	,'00',	'301'	);
insert into tasa_poblacional values ('00238','17',	'1002000039'	,	'86042'		,'00'	);

insert into tasa_poblacional values ('00239','18',	'1002000026'	,	'2586287'	,'00',	'101'	);
insert into tasa_poblacional values ('00240','18',	'1002000027'	,	'1300026'	,'00',	'101'	);
insert into tasa_poblacional values ('00241','18',	'1002000028'	,	'1285962'	,'00',	'101'	);
insert into tasa_poblacional values ('00242','18',	'1002000029'	,	'299'		,'00',	'101'	);
insert into tasa_poblacional values ('00243','18',	'1002000030'	,	'590693'	,'00',	'201'	);
insert into tasa_poblacional values ('00244','18',	'1002000031'	,	'332646'	,'00',	'201'	);
insert into tasa_poblacional values ('00245','18',	'1002000032'	,	'257468'	,'00',	'201'	);
insert into tasa_poblacional values ('00246','18',	'1002000033'	,	'579'		,'00',	'201'	);
insert into tasa_poblacional values ('00247','18',	'1002000034'	,	'29050'		,'00',	'201'	);
insert into tasa_poblacional values ('00248','18',	'1002000035'	,	'16171'		,'00',	'201'	);
insert into tasa_poblacional values ('00249','18',	'1002000036'	,	'12773'		,'00',	'201'	);
insert into tasa_poblacional values ('00250','18',	'1002000037'	,	'106'		,'00',	'201'	);
insert into tasa_poblacional values ('00251','18',	'1002000038'	,	'570954'	,'00',	'301'	);
insert into tasa_poblacional values ('00252','18',	'1002000039'	,	'91285'		,'00',	'302'	);
						
insert into tasa_poblacional values ('00253','19',	'1002000026'	,	'2498880'	,'00',	'101'	);
insert into tasa_poblacional values ('00254','19',	'1002000027'	,	'1262938'	,'00',	'101'	);
insert into tasa_poblacional values ('00255','19',	'1002000028'	,	'1235719'	,'00',	'101'	);
insert into tasa_poblacional values ('00256','19',	'1002000029'	,	'223'		,'00',	'101'	);
insert into tasa_poblacional values ('00257','19',	'1002000030'	,	'602354'	,'00',	'201'	);
insert into tasa_poblacional values ('00258','19',	'1002000031'	,	'338377'	,'00',	'201'	);
insert into tasa_poblacional values ('00259','19',	'1002000032'	,	'263440'	,'00',	'201'	);
insert into tasa_poblacional values ('00260','19',	'1002000033'	,	'537'		,'00',	'201'	);
insert into tasa_poblacional values ('00261','19',	'1002000034'	,	'28956'		,'00',	'201'	);
insert into tasa_poblacional values ('00262','19',	'1002000035'	,	'16151'		,'00',	'201'	);
insert into tasa_poblacional values ('00263','19',	'1002000036'	,	'12671'		,'00',	'201'	);
insert into tasa_poblacional values ('00264','19',	'1002000037'	,	'134'		,'00',	'201'	);
insert into tasa_poblacional values ('00265','19',	'1002000038'	,	'585434'	,'00',	'301'	);
insert into tasa_poblacional values ('00266','19',	'1002000039'	,	'99509'		,'00',	'302'	);



select * from anio;
select * from entidad;
select * from municipio;
select * from indicador;
select * from tasa_poblacional;
select * from nivel;
select * from subnivel;